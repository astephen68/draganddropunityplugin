#include "Wrapper.h"


struct State
{
	Grid* grid;
	World* world;
	std::vector<ICommand*>myCommands;
};

State state;

void Initalize()
{
	state = State();
	state.grid = new Grid();
	state.world = new World();
}

 ICommand * CreateCommand(int x, int y)
{
	 ICommand* command=new Assign(state.grid, x, y);
	 state.myCommands.push_back(command);
	 return command;
}

 void ExecuteCommand(ICommand* command)
 {
	 state.world->ExecuteCommand(command);
 }

 void Undo()
 {
	 state.world->Undo();
 }

void Redo()
 {
	 state.world->Redo();
 }

 bool CheckGridAtPosition(int x, int y)
{
	 return state.grid->GetGrid(x, y);
}





 void CleanUp()
{
	 delete state.grid;
	 for (int i = 0; i < state.myCommands.size(); i++)
	 {
		 delete state.myCommands[i];
	 }
	 delete state.world;
}
