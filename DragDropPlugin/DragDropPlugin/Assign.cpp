#include "Assign.h"

Assign::Assign(Grid* grid,int x,int y)
{
	myGrid = grid;
	this->x = x;
	this->y = y;
}

void Assign::Execute()
{
	myGrid->SetGrid(x, y, true);
}

void Assign::Undo()
{
	myGrid->SetGrid(x, y, false);
}

void Assign::Redo()
{
	Execute();
}

Assign::~Assign()
{
	delete myGrid;
}
