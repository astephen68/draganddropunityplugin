#pragma once
#include <vector>
#include "ICommand.h"
class World
{
public:
	World();
	void ExecuteCommand(ICommand* command);
	void Undo();
	void Redo();
	~World();
private:
	std::vector<ICommand*> OldCommands;
	ICommand* previous=nullptr;
	ICommand* current=nullptr;

};