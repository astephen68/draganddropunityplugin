#pragma once
#include "ICommand.h"
#include "World.h"
#include "Grid.h"

class Assign :public ICommand
{
public:
	Assign( Grid*, int x, int y);
	void Execute() override;
	void Undo() override;
	void Redo() override;
	~Assign();
private:
	Grid* myGrid;
	int x;
	int y;
};