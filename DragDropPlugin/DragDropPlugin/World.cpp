#include "World.h"

World::World()
{
	
}

void World::ExecuteCommand(ICommand * command)
{
	command->Execute();
	OldCommands.push_back(command);
}

void World::Undo()
{
	current = OldCommands.back();
	current->Undo();
	OldCommands.pop_back();
}

void World::Redo()
{
	if (current!=nullptr)
	{
		ExecuteCommand(current);
		current = nullptr;
	}
}




World::~World()
{
	OldCommands.clear();
	if (current!=nullptr)
	{
		current = nullptr;
	}
	if (previous!=nullptr)
	{
		previous = nullptr;
	}

}