#pragma once


#include "LibSettings.h"
#include "World.h"
#include "Grid.h"
#include "Assign.h"

#ifdef __cplusplus
extern "C"
{
#endif

	LIB_API void Initalize();
	LIB_API ICommand* CreateCommand(int x, int y);
	LIB_API void ExecuteCommand(ICommand* commands);
	LIB_API void Undo();
	LIB_API void Redo();
	LIB_API bool CheckGridAtPosition(int x, int y);

	LIB_API void CleanUp();
#ifdef __cplusplus
}
#endif